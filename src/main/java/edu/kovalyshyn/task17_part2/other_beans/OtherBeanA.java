package edu.kovalyshyn.task17_part2.other_beans;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class OtherBeanA implements SomeInterface {
    @Override
    public String name() {
        return "OtherBeanA";
    }
}
