package edu.kovalyshyn.task17_part2.other_beans;

import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

@ToString
public class BeanImpl {
    @Autowired
    private List<SomeInterface> someInterfaceList;

    private OtherBeanA otherBeanA;
    @Autowired
    @Qualifier("otherBeanB")
    private OtherBeanB otherBeanB;
    private OtherBeanC otherBeanC;

    public BeanImpl(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }

    @Autowired
    public void setOtherBeanC(OtherBeanC otherBeanC) {
        this.otherBeanC = otherBeanC;
    }

    public List<SomeInterface> getSomeInterfaceList() {
        return someInterfaceList;
    }


}
