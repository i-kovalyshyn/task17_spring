package edu.kovalyshyn.task17_part2.other_beans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("otherBeanB")
@Order(1)
public class OtherBeanB implements SomeInterface{
    @Override
    public String name() {
        return "OtherBeanB";
    }
}
