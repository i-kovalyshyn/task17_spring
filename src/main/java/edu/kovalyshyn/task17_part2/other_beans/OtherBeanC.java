package edu.kovalyshyn.task17_part2.other_beans;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class OtherBeanC implements SomeInterface {
    @Override
    public String name() {
        return "OtherBeanC";
    }
}
