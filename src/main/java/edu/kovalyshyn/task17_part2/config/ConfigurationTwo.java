package edu.kovalyshyn.task17_part2.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(value = "edu.kovalyshyn.task17_part2", useDefaultFilters = false,
includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
classes = {edu.kovalyshyn.task17_part2.beans2.NarcissusFlower.class,
        edu.kovalyshyn.task17_part2.beans2.RoseFlower.class,
        edu.kovalyshyn.task17_part2.beans3.BeanD.class,
        edu.kovalyshyn.task17_part2.beans3.BeanF.class}))
public class ConfigurationTwo {
}
