package edu.kovalyshyn.task17_part2.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("edu.kovalyshyn.task17_part2.beans1")
public class ConfigurationOne {
}
