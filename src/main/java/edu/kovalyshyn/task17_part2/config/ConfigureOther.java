package edu.kovalyshyn.task17_part2.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "edu.kovalyshyn.task17_part2.other_beans")
public class ConfigureOther {
}
