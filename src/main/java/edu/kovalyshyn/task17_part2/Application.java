package edu.kovalyshyn.task17_part2;

import edu.kovalyshyn.task17_part2.config.ConfigurationOne;
import edu.kovalyshyn.task17_part2.config.ConfigurationTwo;
import edu.kovalyshyn.task17_part2.config.ConfigureOther;
import edu.kovalyshyn.task17_part2.other_beans.BeanImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Log4j2
public class Application {
    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(ConfigurationOne.class,
                        ConfigurationTwo.class,
                        ConfigureOther.class);

        for(String name: context.getBeanDefinitionNames()){
            log.info(name);
        }

        BeanImpl bean = (BeanImpl) context.getBean("beanImpl");
        log.info(bean.toString());

        bean.getSomeInterfaceList().forEach(log::info);

    }
}
