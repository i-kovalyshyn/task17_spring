package edu.kovalyshyn.task17_part2.beans1;

import edu.kovalyshyn.task17_part2.other_beans.OtherBeanB;
import edu.kovalyshyn.task17_part2.other_beans.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanC {
    @Autowired
    private OtherBeanC otherBeanC;

    @Autowired
    @Qualifier("otherBeanB")
    BeanC beanB;

    @Override
    public String toString() {
        return "BeanC{" +
                "otherBeanC=" + otherBeanC +
                ", beanB=" + beanB +
                '}';
    }
}
