package edu.kovalyshyn.task17_part2.beans1;

import edu.kovalyshyn.task17_part2.other_beans.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanA {
    private OtherBeanA otherBeanA;

    @Autowired
    public BeanA(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }
}
