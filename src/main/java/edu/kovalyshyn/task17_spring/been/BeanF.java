package edu.kovalyshyn.task17_spring.been;

import lombok.Data;

@Data
public class BeanF implements BeanValidator {
    private String name;
    private int value;

    @Override
    public boolean validate() {
        return name.isEmpty() || value>=0;
    }
}
