package edu.kovalyshyn.task17_spring.been;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
@Log4j2
@Data

public class BeanB implements BeanValidator {
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private int value;

    private void init() {
        log.info("Bean B init method");
    }

    private void destroy() {
        log.info("Bean B destroy method");
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }
}
