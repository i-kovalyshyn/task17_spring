package edu.kovalyshyn.task17_spring.been;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

@Log4j2
@Data
@NoArgsConstructor

public class BeanA implements InitializingBean, DisposableBean, BeanValidator {
    private String name;
    private int value;

    public BeanA(BeanB beanB, BeanC beanC) {
        this.name = beanB.getName();
        this.value = beanC.getValue();
    }

    public BeanA(BeanB beanB, BeanD beanD) {
        this.name = beanB.getName();
        this.value = beanD.getValue();
    }

    public BeanA(BeanC beanC, BeanD beanD) {
        this.name = beanC.getName();
        this.value = beanD.getValue();
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }

    @Override
    public void destroy() throws Exception {
        log.info("Bean A: destroy()");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Bean A: afterPropertiesSet()");
    }
}
