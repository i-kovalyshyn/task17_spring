package edu.kovalyshyn.task17_spring.been;

public interface BeanValidator {
    boolean validate();
}
