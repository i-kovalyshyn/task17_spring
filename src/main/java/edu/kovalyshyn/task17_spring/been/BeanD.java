package edu.kovalyshyn.task17_spring.been;

import lombok.Data;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;

@ToString
@Data
@Log4j2
public class BeanD implements BeanValidator {
    @Value("${beanD.name}")
    private String name;
    @Value("${beanD.value}")
    private int value;

    private void init() {
        log.info("Bean D init method");
    }

    private void destroy() {
        log.info("Bean D destroy method");
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }
}
