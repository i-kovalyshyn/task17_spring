package edu.kovalyshyn.task17_spring.been;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Log4j2
@Data
public class BeanE implements BeanValidator {
    private String name;
    private int value;

    public BeanE(BeanA beanA) {
        this.name = beanA.getName();
        this.value = beanA.getValue();
    }

    @PostConstruct
    public void postConstruct() {
        log.info("Bean E: postConstruct()");
    }

    @PreDestroy
    public void preDestroy(){
        log.info("Bean E: preDestroy()");
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }
}
