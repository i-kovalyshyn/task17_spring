package edu.kovalyshyn.task17_spring.been;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;

@Data
@Log4j2
public class BeanC implements BeanValidator {
    @Value("${beanC.name}")
    private String name;
    @Value("${beanC.value}")
    private int value;

    private void init() {
        log.info("Bean C init method");
    }

    private void destroy() {
        log.info("Bean C destroy method");
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }
}
