package edu.kovalyshyn.task17_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task17SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Task17SpringApplication.class, args);
    }

}
