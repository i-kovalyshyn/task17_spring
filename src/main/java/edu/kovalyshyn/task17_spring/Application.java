package edu.kovalyshyn.task17_spring;

import edu.kovalyshyn.task17_spring.configure.ConfigureA;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
@Log4j2
public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=
                new AnnotationConfigApplicationContext(ConfigureA.class);
        for (String beanDefinitionName:context.getBeanDefinitionNames()){
            var beanDefinition = context.getBeanDefinition(beanDefinitionName);
            log.info(beanDefinition);
        }
        context.close();
    }
}
