package edu.kovalyshyn.task17_spring.configure;

import edu.kovalyshyn.task17_spring.been.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@Import(ConfigureB.class)
public class ConfigureA {

    @Bean(name="beanD", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = {"beanD", "beanB"})
    public BeanD getBeanD() {
        return new BeanD();
    }

    @Bean(name = "beanB",initMethod = "init", destroyMethod = "destroy")
    public BeanB getBeanB() {
        return new BeanB();
    }

    @Bean(name = "beanC",initMethod = "init", destroyMethod = "destroy")
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(name = "beanA1")
    public BeanA getBeanA(BeanB beanB, BeanC beanC) {
        return new BeanA(beanB, beanC);
    }

    @Bean(name = "beanA2")
    public BeanA getBeanA(BeanB beanB, BeanD beanD) {
        return new BeanA(beanB, beanD);
    }

    @Bean(name = "beanA3")
    public BeanA getBeanA(BeanC beanC, BeanD beanD) {
        return new BeanA(beanC, beanD);
    }

    @Bean
    public BeanE getBeanE(@Qualifier("beanA1") BeanA beanA) {
        return new BeanE(beanA);
    }

    @Bean
    @Lazy
    public BeanF getBeanF() {
        return new BeanF();
    }
}
